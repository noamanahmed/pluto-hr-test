<?php

namespace App\Http\Controllers;

use App\Data;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $db_data=Data::all();       

        return view('welcome',compact('db_data'));
    }
}
