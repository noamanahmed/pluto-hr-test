<?php

namespace App\Http\Middleware;

use Closure;

class SetTimeZone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data=\geoip(\request()->ip())->toArray();
        
        if(array_key_exists('timezone',$data))
        {
            session()->put('user_timezone',$data['timezone']);
            session()->put('user_country',$data['country']);
            session()->put('user_country_code',$data['iso_code']);
        }else{
            session()->put('user_timezone',config('app.timezone'));            
            session()->put('user_country','United States');
            session()->put('user_country_code','US');
        }

        session()->save();

        return $next($request);
    }
}
