<?php

use App\Data;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0;$i<5;$i++)
        {
            $data=new Data();
            $data->timezone=config('app.timezone');
            $data->time=$faker->datetime();
            $data->save();
        }
        
    }
}
