<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {                
                display: flex;
                justify-content: center;
            }

            .flex-left {                
                display: flex;
                justify-content: left;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;               
                
            }
            .bg-image{
                background-image: url("https://images.pexels.com/photos/1037993/pexels-photo-1037993.jpeg");
                object-fit: cover;
                background-size: 170%;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .date-listing{
                margin-top: -30px;
                text-align: left;
            }
        </style>
    </head>
    <body>
        <div class="flex-left position-ref bg-image pl-5">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Plutwo HR Test
                </div>
                <div class="row date-listing">
                    <ul class="list-unstyled">                        
                        @foreach($db_data as  $data)
                        <li><h1> Entry #{{$data->id}}</h1>
                        <ul class="list-unstyled">
                            <li><h2>Your Location : <strong>{{ session()->get('user_country') }} </strong></h2></li>
                            <li><h2>Your TimeZone : <strong>{{ session()->get('user_timezone') }} </strong></h2></li>
                            <li><h2>Server TimeZone : <strong>{{ $data->timezone }} </strong></h2></li>
                            <li><h2>Time From DB : <strong>{{ now()->parse($data->time)->format('H:iA') }} </strong></h2></li>
                            <li><h2>Time in User Timezone : <strong>{{ \Carbon\Carbon::parse($data->time)->setTimezone(session()->get('user_timezone'))->format('H:iA') }} </strong></h2></li>
                        </ul>
                        </li>
                        <hr>
                        @endforeach
                    </ul>
                </div>
                
            </div>
        </div>
    </body>
</html>
